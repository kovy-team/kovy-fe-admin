import React, { Component } from "react";
import { Link } from "react-router-dom";
class Supplier extends Component {
  constructor() {
    super();
    this.state = {
      pagination: [],
      currname: null,
      name: "",
      address: "",
      phone: "",
      email: "",
      fax: "",
      id: "",
      noti: "",
      currType: "add"
    };
  }
  componentWillMount() {
    let tmp = [];
    for (let i = 1; i <= this.props.totalpage; i++) {
      tmp.push(i);
    }
    this.setState({ pagination: tmp });
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.isadd === true) {
      this.reset()
    }
    if (nextProps.isupdate === true) {
      this.reset()
    }
  }
  renderPagination() {
    if (this.state.pagination.length === 0) {
      return null;
    } else {
      return (
        <ul className="pagination pagination-custom col-md-6 offset-md-3">
          <li onClick={() => this.props.backPage()}>
            <a>&laquo;</a>
          </li>
          {this.state.pagination.map((element, index) => {
            if (this.props.page === element) {
              return (
                <li
                  className="active"
                  onClick={() => this.props.setPage(element)}
                >
                  <a>{element}</a>
                </li>
              );
            } else {
              return (
                <li onClick={() => this.props.setPage(element)}>
                  <a>{element}</a>
                </li>
              );
            }
          })}
          <li onClick={() => this.props.nextPage()}>
            <a>&raquo;</a>
          </li>
        </ul>
      );
    }
  }

  isvalidEmail = email => {
    if (
      email.length < 6 ||
      email.indexOf(".") === -1 ||
      email.indexOf("@") === -1
    )
      return false;
    return true;
  };

  isvalidPhone = phone => {
    debugger;
    if (phone.length < 10) return false;
    for (let i = 0; i < phone.length; i++) {
      if (phone.charAt(i) < "0" || phone.charAt(i) > "9") return false;
    }
    return true;
  };

  submitAddSupplier = () => {
    const { name, address, phone, email, fax } = this.state;

    if (name.length <= 0) {
      this.setState({ noti: "Vui lòng nhập tên" });
      return;
    } else {
      this.setState({ noti: "" });
    }

    if (address.length <= 0) {
      this.setState({ noti: "Vui lòng nhập địa chỉ" });
      return;
    } else {
      this.setState({ noti: "" });
    }

    if (!this.isvalidPhone(this.state.phone)) {
      this.setState({ noti: "Số điện thoại không họp lệ" });
      return;
    } else {
      this.setState({ noti: "" });
    }

    if (!this.isvalidEmail(email)) {
      this.setState({ noti: "Email không họp lệ" });
      return;
    } else {
      this.setState({ noti: "" });
    }

    if (!this.isvalidPhone(this.state.phone)) {
      this.setState({ noti: "Fax không họp lệ" });
      return;
    } else {
      this.setState({ noti: "" });
    }
    this.props.addSupplier(name, address, phone, email, fax);
  };

  submitUpdateSupplier = () => {
    const { id, name, address, phone, email, fax } = this.state;

    if (name.length <= 0) {
      this.setState({ noti: "Vui lòng nhập tên" });
      return;
    } else {
      this.setState({ noti: "" });
    }

    if (address.length <= 0) {
      this.setState({ noti: "Vui lòng nhập địa chỉ" });
      return;
    } else {
      this.setState({ noti: "" });
    }

    if (!this.isvalidPhone(this.state.phone)) {
      this.setState({ noti: "Phone không họp lệ" });
      return;
    } else {
      this.setState({ noti: "" });
    }

    if (!this.isvalidEmail(email)) {
      this.setState({ noti: "Email không họp lệ" });
      return;
    } else {
      this.setState({ noti: "" });
    }

    if (!this.isvalidPhone(this.state.phone)) {
      this.setState({ noti: "Fax không họp lệ" });
      return;
    } else {
      this.setState({ noti: "" });
    }
    this.props.updateSupplier(id, name, address, phone, email, fax);
  };

  renderBtn = () => {
    if (this.state.currType === "add") {
      return (
        <div className="form-group">
          <div className="col-lg-offset-2 col-lg-10">
            <button
              onClick={() => this.submitAddSupplier()}
              className="btn-custom"
            >
              Thêm
            </button>
            <button
              disabled
              onClick={() =>
                this.submitUpdateSupplier()
              }
              className="btn-custom"
            >
              Sửa
            </button>
            <button
              onClick={() => this.reset()}
              className="btn-custom"
            >
              Trả lại
            </button>
          </div>
        </div>
      );
    } else {
      return (
        <div className="form-group">
          <div className="col-lg-offset-2 col-lg-10">
            <button
              disabled
              onClick={() => this.props.submitAddSupplier()}
              className="btn-custom"
            >
              Thêm
            </button>
            <button
              onClick={() =>
                this.submitUpdateSupplier()
              }
              className="btn-custom"
            >
              Sửa
            </button>
            <button
              onClick={() => this.reset()}
              className="btn-custom"
            >
              Trả lại
            </button>
          </div>
        </div>
      );
    }
  };
  reset = () => {
    this.setState({
      noti: "",
      id: null,
      name: "",
      address: "",
      phone: "",
      email: "",
      fax: "",
      currType: "add"
    })
  }
  render() {
    return (
      <section id="main-content">
        <div className="row">
          <div className="col-lg-12">
            <h3 className="page-header">
              <i className="fa fa-table" /> Bảng
            </h3>
            <ol className="breadcrumb">
              <li>
                <i className="fa fa-home" />
                <Link to="/">Trang chủ</Link>
              </li>
              <li>
                <i className="fa fa-table" />Bảng
              </li>
              <li>
                <i className="fa fa-th-list" />Quản lý nhà cung cấp
              </li>
            </ol>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <section className="panel">
              <table className="table table-striped table-advance table-hover">
                <tbody>
                  <tr>
                    <th>
                      <i className="icon_profile" /> Tên
                    </th>
                    <th>
                      <i className="icon_profile" /> Địa chỉ
                    </th>
                    <th>
                      <i className="icon_profile" /> Số điện thoại
                    </th>
                    <th>
                      <i className="icon_profile" /> Email
                    </th>
                    <th>
                      <i className="icon_profile" /> Fax
                    </th>
                    <th>
                      <i className="icon_cogs" /> Hoạt động
                    </th>
                  </tr>
                  {this.props.supplier.map((element, index) => {
                    return (
                      <tr>
                        <td>{element.name}</td>
                        <td>{element.address}</td>
                        <td>{element.phone}</td>
                        <td>{element.email}</td>
                        <td>{element.fax}</td>
                        <td>
                          <div className="btn-group">
                            <a
                              onClick={() =>
                                this.setState({
                                  currname: element.name,
                                  name: element.name,
                                  address: element.address,
                                  phone: element.phone,
                                  email: element.email,
                                  fax: element.fax,
                                  id: element.id,
                                  currType: "update"
                                })
                              }
                              className="btn btn-success"
                            ><i className="icon_check_alt2" />
                            </a>
                            <a onClick={() => this.props.deleteSupplier(element.id)} className="btn btn-danger">
                              <i className="icon_close_alt2" />
                            </a>
                          </div>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
              {this.renderPagination()}
            </section>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <section className="panel">
              <header className="panel-heading">Xác thực biểu mẫu</header>
              <div className="panel-body">
                <div className="form">
                  <div className="form-validate form-horizontal">
                    <div className="form-group ">
                      <label for="cname" className="control-label col-lg-2">
                        Tên <span className="required">*</span>
                      </label>
                      <div className="col-lg-10">
                        <input
                          onChange={e => {
                            this.setState({
                              name: e.target.value
                            });
                          }}
                          value={this.state.name}
                          className="form-control"
                          id="cname"
                          name="fullname"
                          minlength="5"
                          type="text"
                          required
                        />
                      </div>
                    </div>
                    <div className="form-group ">
                      <label for="caddress" className="control-label col-lg-2">
                        Địa chỉ <span className="required">*</span>
                      </label>
                      <div className="col-lg-10">
                        <input
                          onChange={e => {
                            this.setState({
                              address: e.target.value
                            });
                          }}
                          value={this.state.address}
                          className="form-control"
                          id="address"
                          name="address"
                          minlength="5"
                          type="text"
                          required
                        />
                      </div>
                    </div>
                    <div className="form-group ">
                      <label for="cphone" className="control-label col-lg-2">
                        Số điện thoại <span className="required">*</span>
                      </label>
                      <div className="col-lg-10">
                        <input
                          onChange={e => {
                            this.setState({
                              phone: e.target.value
                            });
                          }}
                          value={this.state.phone}
                          className="form-control"
                          id="phone"
                          name="phone"
                          minlength="5"
                          type="text"
                          required
                        />
                      </div>
                    </div>
                    <div className="form-group ">
                      <label for="cemail" className="control-label col-lg-2">
                        Email <span className="required">*</span>
                      </label>
                      <div className="col-lg-10">
                        <input
                          onChange={e => {
                            this.setState({
                              email: e.target.value
                            });
                          }}
                          value={this.state.email}
                          className="form-control"
                          id="email"
                          name="email"
                          minlength="5"
                          type="text"
                          required
                        />
                      </div>
                    </div>
                    <div className="form-group ">
                      <label for="cfax" className="control-label col-lg-2">
                        Fax <span className="required">*</span>
                      </label>
                      <div className="col-lg-10">
                        <input
                          onChange={e => {
                            this.setState({
                              fax: e.target.value
                            });
                          }}
                          value={this.state.fax}
                          className="form-control"
                          id="fax"
                          name="fax"
                          minlength="5"
                          type="text"
                          required
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="col-lg-offset-2 col-lg-10">
                        <p>{this.state.noti}</p>
                      </div>
                    </div>
                    {this.renderBtn()}
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>
    );
  }
}
export default Supplier;

import React, { Component } from 'react'
class Navbar extends Component {

    render() {
        return (
            <header className="header dark-bg">
                <div className="toggle-nav">
                    <div className="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i className="icon_menu"></i></div>
                </div>
                <a href="/" className="logo">Kovy <span className="lite">Admin</span></a>
                <div className="top-nav notification-row" style={{ marginRight: "-9%" }}>
                    <a data-toggle="dropdown" className="dropdown-toggle" href="#">
                        <span className="profile-ava">
                            <img style={{ maxWidth: "11%" }} alt="" src="img/admin.png" />
                        </span>
                        <span className="username">    {this.props.user_name}</span>
                    </a>
                    <a style={{ marginLeft: "5%", cursor: "pointer" }} onClick={() => this.props.logout()}><i className="icon_key_alt"></i> Đăng xuất</a>
                </div>
            </header>
        )
    }
}
export default Navbar
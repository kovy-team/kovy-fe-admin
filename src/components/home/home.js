import React, { Component } from "react";
class Home extends Component {
  constructor() {
    super();
    this.state = {
      sum: 1000
    };
  }
  tinh(count) {
    return (count / this.state.sum) * 100 + "%";
  }
  render() {
    return (
      <div>
        <section id="main-content">
          <section className="wrapper">
            <div className="row">
              <div className="col-lg-12">
                <h3 className="page-header">
                  <i className="fa fa-laptop" /> Bảng điều khiển
                </h3>
                <ol className="breadcrumb">
                  <li>
                    <i className="fa fa-home" />
                    <a href="index.html">Trang chủ</a>
                  </li>
                  <li>
                    <i className="fa fa-laptop" />Bảng điều khiển
                  </li>
                </ol>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div className="info-box brown-bg">
                  <i className="fa fa-shopping-cart" />
                  <div className="count">1.324</div>
                  <div className="title">Đã mua</div>
                </div>
              </div>
              <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div className="info-box dark-bg">
                  <i className="fa fa-thumbs-o-up" />
                  <div className="count">876</div>
                  <div className="title">Đặt hàng</div>
                </div>
              </div>

              <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div className="info-box green-bg">
                  <i className="fa fa-cubes" />
                  <div className="count">259</div>
                  <div className="title">Tồn kho</div>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-9 col-md-12">
                <div className="panel panel-default">
                  <div className="panel-heading">
                    <h2>
                      <i className="fa fa-map-marker red" />
                      <strong>Quốc gia</strong>
                    </h2>
                    <div className="panel-actions">
                      <a href="index.html#" className="btn-setting">
                        <i className="fa fa-rotate-right" />
                      </a>
                      <a href="index.html#" className="btn-minimize">
                        <i className="fa fa-chevron-up" />
                      </a>
                      <a href="index.html#" className="btn-close">
                        <i className="fa fa-times" />
                      </a>
                    </div>
                  </div>
                  <div className="panel-body-map">
                    <div id="map" style={{ height: "380px" }} />
                  </div>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-9 col-md-12">
                <div className="panel panel-default">
                  <div className="panel-heading">
                    <h2>
                      <i className="fa fa-flag-o red" />
                      <strong>
                        Top 20 sản phẩm bán chạy nhất
                      </strong>
                    </h2>
                    <div className="panel-actions">
                      <a href="index.html#" className="btn-setting">
                        <i className="fa fa-rotate-right" />
                      </a>
                      <a href="index.html#" className="btn-minimize">
                        <i className="fa fa-chevron-up" />
                      </a>
                      <a href="index.html#" className="btn-close">
                        <i className="fa fa-times" />
                      </a>
                    </div>
                  </div>
                  <div className="panel-body">
                    <table className="table bootstrap-datatable countries">
                      <thead>
                        <tr>
                          {/* <th /> */}
                          <th>Loại sản phẩm</th>
                          <th>Tên sản phẩm</th>
                          <th>Giá</th>
                          <th>Số lượng đã bán</th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.props.top_product.map((element, index) => {
                          return (
                            <tr>
                              {/* <td>
                                <img
                                  src={element.image}
                                  style={{ height: "18px", marginTop: "-2px" }}
                                />
                              </td> */}
                              <td>{element.category_name}</td>
                              <td>{element.name}</td>
                              <td>{element.price}</td>
                              <td>{element.total}</td>
                              {/* <td>
                                <div className="progress thin">
                                  <div
                                    className="progress-bar progress-bar-danger"
                                    role="progressbar"
                                    aria-valuenow="80"
                                    aria-valuemin="0"
                                    aria-valuemax="100"
                                    style={{ width: this.tinh(element.total) }}
                                  />
                                </div>
                                <span className="sr-only">73%</span>
                              </td> */}
                            </tr>
                          )
                        })}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </section>
      </div >
    );
  }
}
export default Home;

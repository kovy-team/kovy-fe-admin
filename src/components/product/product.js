import React, { Component } from "react";
import { Link } from "react-router-dom";
class Product extends Component {
  constructor() {
    super();
    this.state = {
      pagination: [],
      product: null,
      file: null,
      imagePreviewUrl: null,
      curr: "add",
      category: "category",
      supplier: "supplier",
      name: "",
      price: "",
      img: "",
      content: "",
      supplier_id: "",
      category_id: "",
      noti: "",
      total: "",
      id: null
    };
  }
  componentWillMount() {
    let tmp = [];
    for (let i = 1; i <= this.props.totalpage; i++) {
      tmp.push(i);
    }
    this.setState({ pagination: tmp });
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.totalpage !== this.props.totalpage) {
      let tmp = [];
      for (let i = 1; i <= nextProps.totalpage; i++) {
        tmp.push(i);
      }
      this.setState({ pagination: tmp });
    }
    if (nextProps.product !== null) {
      this.setState({
        imagePreviewUrl: nextProps.product.img
      });
    }
    if (nextProps.isadd === true) {
      this.reset()
    }
    if (nextProps.isupdate === true) {
      this.reset()
    }
  }
  renderPagination() {
    if (this.state.pagination.length === 0) {
      return null;
    } else {
      return (
        <ul className="pagination pagination-custom col-md-6 offset-md-3">
          <li onClick={() => this.props.backPage()}>
            <a>&laquo;</a>
          </li>
          {this.state.pagination.map((element, index) => {
            if (this.props.page === element) {
              return (
                <li
                  className="active"
                  onClick={() => this.props.setPage(element)}
                >
                  <a>{element}</a>
                </li>
              );
            } else {
              return (
                <li onClick={() => this.props.setPage(element)}>
                  <a>{element}</a>
                </li>
              );
            }
          })}
          <li onClick={() => this.props.nextPage()}>
            <a>&raquo;</a>
          </li>
        </ul>
      );
    }
  }
  handleChangeImg = img => {
    if (img === undefined)
      return
    let reader = new FileReader();
    reader.onloadend = () => {
      this.setState({
        img: reader.result
      });
    };
    reader.readAsDataURL(img);
    this.getBase64(img).then(value => {
      this.setState({
        file: value
      });
    })
  };
  getBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }
  invalidPrice = t => {
    var str = t.toString();
    let count = 0;
    for (let i = 0; i < str.length; i++) {
      if (str.charAt(i) == "+" || str.charAt(i) == "-") count++;
      else break;
    }
    str = str.substring(count, str.length);
    count = 0;
    for (let i = 0; i < str.length; i++) {
      if (str.charAt(i) == ".") {
        count++;
      }
      if (str.charAt(i) < "0" || str.charAt(i) > "9") return false;
    }
    if (count > 1) return false;
    return !isNaN(Number.parseFloat(str));
  };

  isvalidPTotal = total => {
    for (let i = 0; i < total.length; i++) {
      if (total.charAt(i) < "0" || total.charAt(i) > "9") return false;
    }
    return true;
  };

  submitAddProduct = () => {
    const {
      category_id,
      name,
      price,
      content,
      supplier_id,
      file,
      total
    } = this.state;
    if (name.length <= 0) {
      this.setState({ noti: "Vui lòng nhập tên sản phẩm" });
      return;
    } else {
      this.setState({
        noti: ""
      });
    }
    if (!this.isvalidPTotal(total)) {
      this.setState({
        noti: "Số lượng không hợp lệ"
      });
      return;
    } else {
      this.setState({
        noti: ""
      });
    }
    if (!this.invalidPrice(price)) {
      this.setState({
        noti: "Giá không hợp lệ"
      });
      return;
    } else {
      this.setState({
        noti: ""
      });
    }
    if (category_id === "") {
      this.setState({
        noti: "Vui lòng chọn loại sản phẩm"
      });
      return;
    } else {
      this.setState({
        noti: ""
      });
    }

    if (supplier_id === "") {
      this.setState({
        noti: "Vui lòng chọn nhà cung cấp"
      });
      return;
    } else {
      this.setState({
        noti: ""
      });
    }
    if (file === null) {
      this.setState({
        noti: "Vui lòng chọn hình ảnh"
      });
      return;
    } else {
      this.setState({
        noti: ""
      });
    }
    this.props.addProduct(
      category_id,
      name,
      price,
      content,
      supplier_id,
      file,
      total
    );
  };
  submitUpdateProduct = () => {
    const {
      id,
      category_id,
      name,
      price,
      content,
      supplier_id,
      file,
      total
    } = this.state;
    if (name.length <= 0) {
      this.setState({ noti: "Vui lòng nhập tên sản phẩm" });
      return;
    } else {
      this.setState({
        noti: ""
      });
    }
    if (!this.isvalidPTotal(total)) {
      this.setState({
        noti: "Số lượng không hợp lệ"
      });
      return;
    } else {
      this.setState({
        noti: ""
      });
    }
    if (!this.invalidPrice(price)) {
      this.setState({
        noti: "Giá không hợp lệ"
      });
      return;
    } else {
      this.setState({
        noti: ""
      });
    }
    if (category_id === "") {
      this.setState({
        noti: "Vui lòng chọn loại sản phẩm"
      });
      return;
    } else {
      this.setState({
        noti: ""
      });
    }

    if (supplier_id === "") {
      this.setState({
        noti: "Vui lòng chọn nhà cung cấp"
      });
      return;
    } else {
      this.setState({
        noti: ""
      });
    }
    if (file === null) {
      this.setState({
        noti: "Vui lòng chọn hình ảnh"
      });
      return;
    } else {
      this.setState({
        noti: ""
      });
    }
    this.props.updateProduct(
      id,
      category_id,
      name,
      price,
      content,
      supplier_id,
      file,
      total
    );
  };
  renderBtnSubmit = () => {
    if (this.state.curr === "add") {
      return (
        <div className="form-group">
          <div className="col-lg-offset-2 col-lg-10">
            <button
              onClick={() => this.submitAddProduct()}
              className="btn-custom"
              type="submit"
            >
              Thêm
            </button>
            <button className="btn-custom" disabled type="button">
              Sửa
            </button>
            <button className="btn-custom" onClick={() => this.reset()}>Trả lại</button>
          </div>
        </div>
      );
    } else {
      return (
        <div className="form-group">
          <div className="col-lg-offset-2 col-lg-10">
            <button className="btn-custom" disabled type="submit">
              Thêm
            </button>
            <button
              className="btn-custom"
              onClick={() => this.submitUpdateProduct()}
              type="button"
            >
              Sửa
            </button>
            <button className="btn-custom" onClick={() => this.reset()}>Trả lại</button>
          </div>
        </div>
      );
    }
  };
  reset = () => {
    this.setState({
      noti: "",
      name: "",
      file: null,
      imagePreviewUrl: null,
      curr: "add",
      category: "category",
      supplier: "supplier",
      name: "",
      price: "",
      img: "",
      content: "",
      supplier_id: "",
      category_id: "",
      noti: "",
      total: "",
      id: null
    })
  }
  renderMenuCategory = () => {
    if (this.props.category) {
      return this.props.category.map((element, index) => {
        return (
          <li
            onClick={() =>
              this.setState({
                category: element.name,
                category_id: element.id
              })
            }
          >
            <a>{element.name}</a>
          </li>
        );
      });
    } else {
      return null;
    }
  };
  renderMenuSupplier = () => {
    if (this.props.supplier) {
      return this.props.supplier.map((element, index) => {
        return (
          <li
            onClick={() =>
              this.setState({ supplier: element.name, supplier_id: element.id })
            }
          >
            <a>{element.name}</a>
          </li>
        );
      });
    } else {
      return null;
    }
  };
  getNameCategoryByID = id => {
    for (let i = 0; i < this.props.category.length; i++) {
      if (id === this.props.category[i].id) {
        return this.props.category[i].name;
      }
    }
  };
  getNameSupplierByID = id => {
    for (let i = 0; i < this.props.supplier.length; i++) {
      console.log(id + " === " + this.props.supplier[i].id);
      if (id === this.props.supplier[i].id)
        return this.props.supplier[i].name;
    }
  };
  render() {
    return (
      <section id="main-content">
        <div className="row">
          <div className="col-lg-12">
            <h3 className="page-header">
              <i className="fa fa-table" /> Bảng
            </h3>
            <ol className="breadcrumb">
              <li>
                <i className="fa fa-home" />
                <Link to="/">Trang chủ</Link>
              </li>
              <li>
                <i className="fa fa-table" />Bảng
              </li>
              <li>
                <i className="fa fa-th-list" />Quản lý sản phẩm
              </li>
            </ol>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <section className="panel">
              <table className="table table-striped table-advance table-hover">
                <tbody>
                  <tr>
                    <th>
                      <i className="icon_profile" /> Loại sản phẩm
                    </th>
                    <th>
                      <i className="icon_profile" /> Tên sản phẩm
                    </th>
                    <th>
                      <i className="icon_mail_alt" /> Giá
                    </th>
                    <th>
                      <i className="icon_pin_alt" /> Mô tả
                    </th>
                    <th>
                      <i className="icon_pin_alt" /> Số lượng
                    </th>
                    <th>
                      <i className="icon_cogs" /> Hoạt động
                    </th>
                  </tr>
                  {this.props.product.map((element, index) => {
                    return (
                      <tr>
                        <td>{element.categoryName}</td>
                        <td>{element.product}</td>
                        <td>{element.price}</td>
                        <td style={{ width: "40%" }}>{element.content}</td>
                        <td>{element.total}</td>
                        <td>
                          <div className="btn-group">
                            <a
                              onClick={() =>
                                this.setState({
                                  curr: "update",
                                  name: element.product,
                                  price: element.price,
                                  content: element.content,
                                  category: this.getNameCategoryByID(
                                    element.category_id
                                  ),
                                  category_id: element.category_id,
                                  supplier_id: element.supplier_id,
                                  supplier: this.getNameSupplierByID(
                                    element.supplier_id
                                  ),
                                  img: element.image,
                                  id: element.id,
                                  total: element.total
                                })
                              }
                              className="btn btn-success"> <i className="icon_check_alt2" />
                            </a>
                            <a onClick={() => this.props.deleteProduct(element.id)} className="btn btn-danger">
                              <i className="icon_close_alt2" />
                            </a>
                          </div>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
              {this.renderPagination()}
            </section>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <section className="panel">
              <header className="panel-heading">Biểu mẩu xác thực</header>
              <div className="panel-body">
                <div className="form" id="from-book">
                  <div
                    className="form-validate form-horizontal"
                    id="feedback_form"
                    method="get"
                    action=""
                  >
                    <div className="form-group ">
                      <label for="cname" className="control-label col-lg-2">
                        Tên sản phẩm <span className="required">*</span>
                      </label>
                      <div className="col-lg-10">
                        <input
                          onChange={e => {
                            this.setState({
                              name: e.target.value
                            });
                          }}
                          value={this.state.name}
                          className="form-control"
                          id="cname"
                          name="fullname"
                          minlength="5"
                          type="text"
                          required
                        />
                      </div>
                    </div>
                    <div className="form-group ">
                      <label for="curl" className="control-label col-lg-2">
                        Giá
                      </label>
                      <div className="col-lg-10">
                        <input
                          value={this.state.price}
                          onChange={e =>
                            this.setState({
                              price: e.target.value
                            })
                          }
                          className="form-control "
                          id="curl"
                          type="text"
                          name="url"
                        />
                      </div>
                    </div>
                    <div className="form-group ">
                      <label for="cname" className="control-label col-lg-2">
                        Mô tả <span className="required">*</span>
                      </label>
                      <div className="col-lg-10">
                        <input
                          value={this.state.content}
                          onChange={e =>
                            this.setState({
                              content: e.target.value
                            })
                          }
                          className="form-control"
                          id="subject"
                          name="subject"
                          minlength="5"
                          type="text"
                          required
                        />
                      </div>
                    </div>
                    <div className="form-group ">
                      <label for="cname" className="control-label col-lg-2">
                        Số lượng <span className="required">*</span>
                      </label>
                      <div className="col-lg-10">
                        <input
                          value={this.state.total}
                          onChange={e =>
                            this.setState({
                              total: e.target.value
                            })
                          }
                          className="form-control"
                          id="subject"
                          name="subject"
                          type="text"
                          required
                        />
                      </div>
                    </div>
                    <div className="form-group ">
                      <label for="comment " className="control-label col-lg-2">
                        Loại sản phẩm
                      </label>
                      <div className="btn-group col-lg-10">
                        <button
                          style={{ width: "200px" }}
                          type="button"
                          className="btn btn-default dropdown-toggle"
                          data-toggle="dropdown"
                        >
                          {this.state.category} <span className="caret" />
                        </button>
                        <ul className="dropdown-menu" role="menu">
                          {this.renderMenuCategory()}
                        </ul>
                      </div>
                    </div>
                    <div className="form-group ">
                      <label for="comment" className="control-label col-lg-2">
                        Nhà cung cấp
                      </label>
                      <div className="btn-group col-lg-10">
                        <button
                          type="button"
                          className="btn btn-default dropdown-toggle"
                          data-toggle="dropdown"
                          style={{ width: "200px" }}
                        >
                          {this.state.supplier} <span className="caret" />
                        </button>
                        <ul className="dropdown-menu" role="menu">
                          {this.renderMenuSupplier()}
                        </ul>
                      </div>
                    </div>
                    <div className="form-group ">
                      <label for="comment" className="control-label col-lg-2">
                        Tải hình ảnh{" "}
                      </label>
                      <div className="col-lg-10">
                        <input
                          className="form-control "
                          type="file"
                          id="ccomment"
                          name="comment"
                          required
                          onChange={e =>
                            this.handleChangeImg(e.target.files[0])
                          }
                        />
                      </div>
                    </div>
                    <div className="form-group ">
                      <label for="comment" className="control-label col-lg-2">
                        Hình ảnh
                      </label>
                      <div className="col-lg-10">
                        <img
                          src={this.state.img}
                          style={{ maxWidth: "300px" }}
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="col-lg-offset-2 col-lg-10">
                        <p>{this.state.noti}</p>
                      </div>
                    </div>
                    {this.renderBtnSubmit()}
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>
    );
  }
}
export default Product;

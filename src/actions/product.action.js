import axios from 'axios'
import { productTypes } from '../constants/action.types'
export const getProduct = () => async (dispatch, getState) => {
    let res
    try {
        //TODO
        // res = await axios.get('http://localhost:8000/api/products/product')

        res = await axios.post('http://localhost:8000/api/getProductAll', {
            page: getState().productReducers.product.page
        })
    }
    catch (err) {
        return
    }
    dispatch(setProduct(res.data.data))
    dispatch(setTotalPage(res.data.totalPage))
}
export const setProduct = (data) => ({
    type: productTypes.SET_PRODUCT,
    data
})
export const setPage = (page) => ({
    type: productTypes.SET_PAGE,
    page
})
export const setTotalPage = (totalpage) => ({
    type: productTypes.SET_TOTAL_PAGE,
    totalpage
})
export const categorySetPage = (page) => ({
    type: productTypes.CATEGORY_SET_PAGE,
    page
})
export const categorySetTotalPage = (totalpage) => ({
    type: productTypes.CATEGORY_SET_TOTAL_PAGE,
    totalpage
})
export const publisherSetPage = (page) => ({
    type: productTypes.PUBLISHER_SET_PAGE,
    page
})
// export const publisherSetTotalPage = (totalpage) => ({
//     type: productTypes.PUBLISHER_SET_TOTAL_PAGE,
//     totalpage
// })
export const deleteProduct = (id) => async (dispatch, getState) => {
    let res
    try {
        res = await axios.delete('http://localhost:8000/api/deleteProduct/' + id)
    }
    catch (err) {
        return
    }
    dispatch(getProduct())
}

export const nextPage = () => (dispatch, getState) => {
    let page = getState().productReducers.product.page
    let totalpage = getState().productReducers.product.totalpage
    if (page < totalpage) {
        dispatch(setPage(parseInt(page) + 1))
    }
}

export const deleteSupplier = (id) => async (dispatch, getState) => {
    let res
    try {
        res = await axios.delete('http://localhost:8000/api/deleteSupplier/' + id)
    }
    catch (err) {
        return
    }
    dispatch(getSupplier())
}

export const getCategory = () => async (dispatch, getState) => {
    let res
    try {
        res = await axios.get('http://localhost:8000/api/category')
    }
    catch (err) {
        return
    }
    dispatch(setCategory(res.data))
    dispatch(categorySetTotalPage(res.data.totalPage))
}

export const getSupplier = () => async (dispatch, getState) => {
    let res
    try {
        res = await axios.get('http://localhost:8000/api/supplier')
    }
    catch (err) {
        return
    }
    dispatch(setSupplier(res.data))
    // dispatch(publisherSetTotalPage(res.data.totalPage))
}

export const setCategory = (data) => ({
    type: productTypes.SET_CATEGORY_BOOK,
    data
})

export const setSupplier = (data) => ({
    type: productTypes.SET_SUPPLIER,
    data
})
export const addCategorySuccess = () => ({
    type: productTypes.ADD_CATEGORY_SUCCESS
})
export const addCategotyFail = () => ({
    type: productTypes.ADD_CATEGORY_FAIL
})
export const updateCategorySuccess = () => ({
    type: productTypes.UPDATE_CATEGORY_SUCCESS
})
export const updateCategoryFail = () => ({
    type: productTypes.UPDATE_CATEGORY_FAIL
})
export const resetCategory = () => ({
    type: productTypes.RESET_CATEGORY
})
export const addCategory = (name) => async (dispatch, getState) => {
    dispatch(resetCategory())
    let res
    try {
        res = await axios.post('http://localhost:8080/admin/addcategory', {
            name: name
        })
    }
    catch (err) {
        dispatch(addCategotyFail())
        return
    }
    dispatch(addCategorySuccess())
    dispatch(getCategory())
}

export const updateCategory = (id, name) => async (dispatch, getState) => {
    let res
    try {
        res = await axios.post('http://localhost:8080/admin/updatecategory', {
            id: id,
            name: name
        })
    }
    catch (err) {
        dispatch(updateCategoryFail())
        return
    }
    dispatch(updateCategorySuccess())
    dispatch(getCategory())
}
export const addSupplierSuccess = () => ({
    type: productTypes.ADD_SUPPLIER_SUCCESS
})
export const addSupplierFail = () => ({
    type: productTypes.ADD_SUPPLIER_FAIL
})
export const updateSupplierSuccess = () => ({
    type: productTypes.UPDATE_SUPPLIER_SUCCESS
})
export const updateSupplierFail = () => ({
    type: productTypes.UPDATE_SUPPLIER_FAIL
})
export const resetSupplier = () => ({
    type: productTypes.RESET_SUPPLIER
})
export const addSupplier = (name, address, phone, email, fax) => async (dispatch, getState) => {
    dispatch(resetSupplier())
    let res
    try {
        res = await axios.post('http://localhost:8000/api/addSupplier', {
            name: name,
            address: address,
            phone: phone,
            email: email,
            fax: fax
        })
    }
    catch (err) {
        dispatch(addSupplierFail())
        return
    }
    dispatch(addSupplierSuccess())
    dispatch(getSupplier())
}

export const updateSupplier = (id, name, address, phone, email, fax) => async (dispatch, getState) => {
    let res
    try {
        res = await axios.put('http://localhost:8000/api/updateSupplier', {
            id: id,
            name: name,
            address: address,
            phone: phone,
            email: email,
            fax: fax
        })
    }
    catch (err) {
        dispatch(updateSupplierFail())
        return
    }
    dispatch(updateSupplierSuccess())
    dispatch(getSupplier())
}
export const backPage = () => (dispatch, getState) => {
    let page = getState().productReducers.product.page
    if (page > 1) {
        dispatch(setPage(parseInt(page) - 1))
    }
}
export const categoryBackPage = () => (dispatch, getState) => {
    let page = getState().productReducers.category.page
    if (page > 1) {
        dispatch(categorySetPage(parseInt(page) - 1))
    }
}

export const categoryNextPage = () => (dispatch, getState) => {
    let page = getState().productReducers.category.page
    let totalpage = getState().productReducers.category.totalpage
    if (page < totalpage) {
        dispatch(categorySetPage(parseInt(page) + 1))
    }
}
export const publisherBackPage = () => (dispatch, getState) => {
    let page = getState().productReducers.publisher.page
    if (page > 1) {
        dispatch(publisherSetPage(parseInt(page) - 1))
    }
}

export const publisherNextPage = () => (dispatch, getState) => {
    let page = getState().productReducers.publisher.page
    let totalpage = getState().productReducers.publisher.totalpage
    if (page < totalpage) {
        dispatch(publisherSetPage(parseInt(page) + 1))
    }
}
export const billBackPage = () => (dispatch, getState) => {
    let page = getState().productReducers.bill.page
    if (page > 1) {
        dispatch(billSetPage(parseInt(page) - 1))
    }
}

export const billNextPage = () => (dispatch, getState) => {
    let page = getState().productReducers.bill.page
    let totalpage = getState().productReducers.bill.totalpage
    if (page < totalpage) {
        dispatch(billSetPage(parseInt(page) + 1))
    }
}
export const addProductSuccess = () => ({
    type: productTypes.ADD_PRODUCT_SUCCESS
})
export const addProductFail = () => ({
    type: productTypes.ADD_PRODUCT_FAIL
})
export const updateProductSuccess = () => ({
    type: productTypes.UPDATE_PRODUCT_SUCCESS
})
export const updateProductFail = () => ({
    type: productTypes.UPDATE_PRODUCT_FAIL
})
export const addProduct = (category_id, name, price, content, supplier_id, file, total) =>
    async (dispatch, getState) => {
        let res
        try {
            res = await axios.post('http://localhost:8000/api/addProduct',
                {
                    category_id: category_id,
                    name: name,
                    price: price,
                    content: content,
                    supplier_id: supplier_id,
                    image: file,
                    total: total,
                })
        }
        catch (err) {
            dispatch(addProductFail())
            return
        }
        dispatch(addProductSuccess())
        dispatch(getProduct())
    }
export const updateProduct = (id, category_id, name, price, content, supplier_id, file, total) => async (dispatch, getState) => {
    try {
        let res
        res = await axios.put('http://localhost:8000/api/updateProduct',
            {
                id: id,
                category_id: category_id,
                name: name,
                price: price,
                content: content,
                supplier_id: supplier_id,
                image: file,
                total: total,
            }
        )
    }
    catch (err) {
        dispatch(updateProductFail())
        return
    }
    dispatch(updateProductSuccess())
    dispatch(getProduct())
}
export const setBill = (data) => ({
    type: productTypes.BILL_SET_DATA,
    data
})
export const billSetPage = (page) => ({
    type: productTypes.BILL_SET_PAGE,
    page
})
export const billSetTotalPage = (totalpage) => ({
    type: productTypes.BILL_SET_TOTAL_PAGE,
    totalpage
})
export const getBill = (status) => async (dispatch, getState) => {
    let link = "http://localhost:8080/bill/status/true"
    if (status === "false") {
        link = "http://localhost:8080/bill/status/false"
    }
    let res = null
    try {
        res = await axios.get(link)
    }
    catch (err) {
        return
    }
    dispatch(setBill(res.data.data))
    dispatch(billSetTotalPage(res.data.totalPage))

}
import axios from 'axios'
import { homeTypes } from '../constants/action.types'
export const setTopProduct = (data) => ({
    type: homeTypes.SET_TOP_PRODUCT,
    data
})
export const getTopProduct = () => async (dispatch, getState) => {
    let res = null
    try {
        res = await axios.get('http://localhost:8000/api/topProduct')
    }
    catch (err) {
        return
    }
    dispatch(setTopProduct(res.data))
}
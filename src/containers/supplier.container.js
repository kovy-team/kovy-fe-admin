import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as productActions from "../actions/product.action";
import Supplier from "../components/supplier/supplier";
import NavbarContainer from "./navbar.container";
import Slider from "./slider.container";
import * as userActions from "../actions/user.action";
class SupplierContainer extends Component {
  constructor() {
    super();
  }
  async componentWillMount() {
    this.props.productActions.getSupplier();
    let res = await this.props.userActions.auth()
    if (res === false)
      this.props.history.push('/login')
  }
  componentWillReceiveProps(nextProps) {
    if (
      nextProps.islogin !== this.props.islogin &&
      nextProps.islogin === false
    ) {
      this.props.history.push("/login");
    }
    if (nextProps.page !== this.props.page) {
      this.props.productActions.getSupplier();
    }
  }
  render() {
    return (
      <section id="container" className="">
        <NavbarContainer />
        <Slider />
        <Supplier
          supplier={this.props.supplier}
          isadd={this.props.isadd}
          addSupplier={(name, address, phone, email, fax) =>
            this.props.productActions.addSupplier(name, address, phone, email, fax)}
          updateSupplier={(id, name, address, phone, email, fax) =>
            this.props.productActions.updateSupplier(id, name, address, phone, email, fax)
          }
          deleteSupplier={id => this.props.productActions.deleteSupplier(id)}
          isupdate={this.props.isupdate}
          page={this.props.page}
          totalpage={this.props.totalpage}
          backPage={() => this.props.productActions.publisherBackPage()}
          nextPage={() => this.props.productActions.publisherNextPage()}
          setPage={page => this.props.productActions.publisherSetPage(page)}
        />
      </section>
    );
  }
}
const mapStateToProps = state => ({
  supplier: state.productReducers.supplier.data,
  isadd: state.productReducers.supplier.isadd,
  isupdate: state.productReducers.supplier.isupdate,
  totalpage: state.productReducers.supplier.totalpage,
  page: state.productReducers.supplier.page,
  islogin: state.userReducers.user.islogin
});

const mapDispatchToProps = dispatch => {
  return {
    productActions: bindActionCreators(productActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch)
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SupplierContainer);

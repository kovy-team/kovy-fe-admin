import React, { Component } from 'react'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as userActions from "../actions/user.action";
import Navbar from '../components/navbar/navbar'
import storeConfig from '../config/store.config'
class NavbarContainer extends Component {
    constructor() {
        super();
        this.state = {
            username: ''
        }
    }
    componentWillMount() {
        if (storeConfig) {
            this.setState({ username: storeConfig.getUser().username })
        }
    }

    render() {
        return (
            <div>
                <Navbar
                    logout={() => this.props.userActions.logout()}
                    user_name={this.state.username}
                />
            </div>
        )
    }

}
const mapStateToProps = state => ({
    islogin: state.userReducers.user.islogin
})

const mapDispatchToProps = dispatch => {
    return ({
        userActions: bindActionCreators(userActions, dispatch)
    })
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NavbarContainer)
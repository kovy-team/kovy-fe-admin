import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import HomeContainer from "./home.container";
import ProductContainer from "./product.container";
import CategoryContainer from "./category.container";
import SupplierContainer from "./supplier.container";
import UserContainer from "./user.container";
import LoginContainer from "./login.container";
import StatisticalContainer from './statistical.container'
import BillContainer from './bill.container'
class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={HomeContainer} />
          <Route exact path="/quan-ly-san-pham" component={ProductContainer} />
          <Route exact path="/quan-ly-loai-san-pham" component={CategoryContainer} />
          <Route exact path="/quan-ly-nha-cung-cap" component={SupplierContainer} />
          <Route exact path="/quan-ly-nguoi-dung" component={UserContainer} />
          <Route exact path="/login" component={LoginContainer} />
          {/* <Route exact path="/statistical" component={StatisticalContainer} />
          <Route exact path="/billmanager" component={BillContainer} /> */}
        </Switch>
      </Router>
    );
  }
}
export default App;

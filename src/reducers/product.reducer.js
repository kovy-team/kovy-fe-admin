import { productTypes } from '../constants/action.types'
import { combineReducers } from 'redux'
const category = (state = { data: [], page: 1, totalpage: null }, action) => {
    switch (action.type) {
        case productTypes.SET_CATEGORY_BOOK: {
            return {
                ...state,
                data: action.data
            }
        }
        case productTypes.ADD_CATEGORY_SUCCESS: {
            return {
                ...state,
                isadd: true
            }
        }
        case productTypes.ADD_CATEGORY_FAIL: {
            return {
                ...state,
                isadd: false
            }
        }
        case productTypes.UPDATE_CATEGORY_SUCCESS: {
            return {
                ...state,
                isupdate: true
            }
        }
        case productTypes.UPDATE_CATEGORY_FAIL: {
            return {
                ...state,
                isupdate: false
            }
        }
        case productTypes.RESET_CATEGORY: {
            return {
                ...state,
                isadd: null,
                isupdate: null
            }
        }
        case productTypes.CATEGORY_SET_PAGE: {
            return {
                ...state,
                page: action.page
            }
        }
        // case productTypes.CATEGORY_SET_TOTAL_PAGE: {
        //     return {
        //         ...state,
        //         totalpage: action.totalpage
        //     }
        // }
        default: return state
    }
}
const supplier = (state = { data: [], page: 1, totalpage: null }, action) => {
    switch (action.type) {
        case productTypes.SET_SUPPLIER: {
            return {
                ...state,
                data: action.data
            }
        }
        case productTypes.ADD_SUPPLIER_SUCCESS: {
            return {
                ...state,
                isadd: true
            }
        }
        case productTypes.ADD_SUPPLIER_FAIL: {
            return {
                ...state,
                isadd: false
            }
        }
        case productTypes.UPDATE_SUPPLIER_SUCCESS: {
            return {
                ...state,
                isupdate: true
            }
        }
        case productTypes.UPDATE_SUPPLIER_FAIL: {
            return {
                ...state,
                isupdate: false
            }
        }
        case productTypes.RESET_SUPPLIER: {
            return {
                ...state,
                isadd: null,
                isupdate: null
            }
        }
        case productTypes.PUBLISHER_SET_PAGE: {
            return {
                ...state,
                page: action.page
            }
        }
        case productTypes.PUBLISHER_SET_TOTAL_PAGE: {
            return {
                ...state,
                totalpage: action.totalpage
            }
        }
        default: return state
    }
}
const bill = (state = { data: [], page: 1, totalpage: null }, action) => {
    switch (action.type) {
        case productTypes.BILL_SET_PAGE: {
            return {
                ...state,
                page: action.page
            }
        }
        case productTypes.BILL_SET_TOTAL_PAGE: {
            return {
                ...state,
                totalpage: action.totalpage
            }
        }
        case productTypes.BILL_SET_DATA: {
            return {
                ...state,
                data: action.data
            }
        }
        default: return state
    }
}
const product = (state = {
    data: [], page: 1, totalpage: null
}, action) => {
    switch (action.type) {
        case productTypes.SET_PRODUCT: {
            return {
                ...state,
                data: action.data
            }
        }
        case productTypes.SET_PAGE: {
            return {
                ...state,
                page: action.page
            }
        }
        case productTypes.SET_TOTAL_PAGE: {
            return {
                ...state,
                totalpage: action.totalpage
            }
        }
        case productTypes.ADD_PRODUCT_SUCCESS: {
            return {
                ...state,
                isadd: true
            }
        }
        case productTypes.ADD_PRODUCT_FAIL: {
            return {
                ...state,
                isadd: false
            }
        }
        case productTypes.UPDATE_PRODUCT_SUCCESS: {
            return {
                ...state,
                isupdate: true
            }
        }
        case productTypes.UPDATE_PRODUCT_FAIL: {
            return {
                ...state,
                isupdate: false
            }
        }
        case productTypes.RESET_PRODUCT: {
            return {
                ...state,
                isadd: null,
                isupdate: null
            }
        }
        default: return state
    }
}
export default combineReducers({
    category,
    supplier,
    product,
    bill
})